package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/")
	String home() {
		String unusedVariable;
		return "Spring is here!";
	}
	@GetMapping("/coucou")
	String coucou() throws Exception {
		try {
			int i = 1/0;
		} catch (Exception e) {
			throw new Exception();
		}
		return "Spring is here!";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}